<?php

namespace Database\Seeders;

use App\Models\Record;
use Illuminate\Database\Seeder;

class RecordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Record::create(['plate'=>'aaa1122', 'type_id'=>2]);
        Record::create(['plate'=>'bbb1122', 'type_id'=>2]);
        Record::create(['plate'=>'ccc1122', 'type_id'=>2]);
        Record::create(['plate'=>'eee1122', 'type_id'=>2]);
        Record::create(['plate'=>'fff1122', 'type_id'=>2]);
        Record::create(['plate'=>'ggg1122', 'type_id'=>2]);
        Record::create(['plate'=>'hhh1122', 'type_id'=>2]);
        Record::create(['plate'=>'jjj1122', 'type_id'=>2]);
        Record::create(['plate'=>'kkk1122', 'type_id'=>2]);
        Record::create(['plate'=>'lll1122', 'type_id'=>2]);
        Record::create(['plate'=>'zzz8899', 'type_id'=>3]);
        Record::create(['plate'=>'yyy8899', 'type_id'=>3]);
        Record::create(['plate'=>'xxx8899', 'type_id'=>3]);
        Record::create(['plate'=>'www8899', 'type_id'=>3]);
        Record::create(['plate'=>'vvv8899', 'type_id'=>3]);
    }
}
