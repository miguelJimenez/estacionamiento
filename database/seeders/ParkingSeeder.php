<?php

namespace Database\Seeders;

use App\Models\Parking;
use Illuminate\Database\Seeder;

class ParkingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $slots = env('PARKING_SLOTS', 20);
        for ($i=1; $i <= $slots ; $i++) { 
            $parking = new Parking();
            $parking->slot = $i;
            $parking->save();
        }
    }
}
