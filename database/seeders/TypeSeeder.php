<?php

namespace Database\Seeders;

use App\Models\Type;
use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $default = new Type();
        $default->type = 'dafault';
        $default->cost = 3;
        $default->default = true;
        $default->save();

        $oficial = new Type();
        $oficial->type = 'Oficial';
        $oficial->cost = 0;
        $oficial->save();

        $residente = new Type();
        $residente->type = 'Residente';
        $residente->cost = 1;
        $residente->save();
    }
}
