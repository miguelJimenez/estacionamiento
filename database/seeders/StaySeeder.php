<?php

namespace Database\Seeders;

use App\Models\Stay;
use Illuminate\Database\Seeder;

class StaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Stay::factory()->count(50)->create();
    }
}
