<?php

namespace Database\Factories;

use App\Models\Stay;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class StayFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Stay::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $entrada = $this->faker->dateTimeThisMonth($max = 'now');
        $time = Carbon::parse($entrada);
        $minutos = rand(6,150);
        $salida = $time->addMinutes($minutos);
        $tipo = $this->faker->randomElement($array = array ('default','Oficial','Residente'));
        $total = 0;
        if($tipo == 'default') {
            $total = $minutos * 3;
        }
        if($tipo == 'Residente') {
            $total = $minutos * 1;
        }
        return [
            'plate' => Str::random(7),
            'in' => $entrada,
            'out' => $salida,
            'type' => $tipo, // password
            'cost' => $total
        ];
    }
}
