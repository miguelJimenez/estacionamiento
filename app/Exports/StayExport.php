<?php

namespace App\Exports;

use App\Models\Stay;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromView;

class StayExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $request;

    public function __construct(Request $request) {
        $this->request= $request;
    }

    public function view(): View
    {
        $stays = Stay::where('in', '>=', $this->request->start)->where('out', '<=', $this->request->end)->get();
        return view('stay.xlsx', compact('stays'));
    }
}
