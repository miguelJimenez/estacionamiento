<?php

namespace App\Http\Controllers\Stay;

use App\Exports\StayExport;
use App\Http\Controllers\Controller;
use App\Models\Stay;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReportStayController extends Controller
{
    public function downloadXls(Request $request)
    {
        return Excel::download(new StayExport($request), 'reporte.xlsx');
    }

    public function downloadPdf(Request $request)
    {
        $stays = Stay::where('in', '>=', $request->start)->where('out', '<=', $request->end)->get();
        $pdf = \PDF::loadView('stay.pdf', compact('stays'));
        return $pdf->stream('reporte.pdf');
    }
}
