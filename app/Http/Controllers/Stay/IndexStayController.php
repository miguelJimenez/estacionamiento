<?php

namespace App\Http\Controllers\Stay;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndexStayController extends Controller
{
    public function index()
    {
        return view('stay.index');
    }
}
