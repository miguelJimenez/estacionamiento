<?php

namespace App\Http\Controllers\Parking;

use App\Http\Controllers\Controller;
use App\Models\Parking;
use App\Models\Record;
use Illuminate\Http\Request;

class InitParkingController extends Controller
{
    public function initParking(Request $request)
    {
        $parking = Parking::where('slot', $request->slot)->whereNull('plate')->first();
        if ($parking) {
            $parking->plate = $request->placa;
            $parking->started_at = now();
            $parking->save();
        }
        return redirect()->route('index');
    }
}
