<?php

namespace App\Http\Controllers\Parking;

use App\Http\Controllers\Controller;
use App\Models\Parking;
use App\Models\Record;
use App\Models\Stay;
use App\Models\Type;
use Illuminate\Http\Request;

class FinishParkingController extends Controller
{
    public function finishParking(Request $request)
    {
        $parking = Parking::where('slot', $request->slot)->first();
        $record = Record::with('type')->where('plate', $parking->plate)->first();
        $type = null;
        if ($record) {
            $type = $record->type;
        } else {
            $type = Type::where('default', true)->first();
        }

        $stay = new Stay();
        $stay->plate = $parking->plate;
        $stay->in = $parking->started_at;
        $stay->out = now();
        $stay->type = $type->type;
        $minutos = $stay->out->diffInMinutes($stay->in);
        $stay->cost = $type->cost * $minutos;
        $stay->save();

        $parking->plate = null;
        $parking->started_at= null;
        $parking->save();

        return back()->with('cobrar', $stay);
    }
}
