<?php

namespace App\Http\Controllers\Parking;

use App\Http\Controllers\Controller;
use App\Models\Parking;
use Illuminate\Http\Request;

class ParkingIndexController extends Controller
{
    public function index()
    {
        $parkings = Parking::all();
        return view('index', compact('parkings'));
    }
}
