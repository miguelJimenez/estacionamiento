<?php

namespace App\Http\Controllers\datatables;

use App\Http\Controllers\Controller;
use App\Models\Stay;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class StayDatatable extends Controller
{
    public function stayDatatable() 
    {
        $query = Stay::all();
        return Datatables::of($query)->make(true);
    }
}
