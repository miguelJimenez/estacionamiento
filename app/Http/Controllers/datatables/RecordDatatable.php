<?php

namespace App\Http\Controllers\datatables;

use App\Http\Controllers\Controller;
use App\Models\Record;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class RecordDatatable extends Controller
{
    public function recordDatatable()
    {
        $query = Record::with('type')->get();
        return Datatables::of($query)->make(true);
    }
}
