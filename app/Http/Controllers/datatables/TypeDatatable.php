<?php

namespace App\Http\Controllers\datatables;

use App\Http\Controllers\Controller;
use App\Models\Type;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class TypeDatatable extends Controller
{
    public function typeDatatable()
    {
        return Datatables::of(Type::query())->make(true);
    }
}
