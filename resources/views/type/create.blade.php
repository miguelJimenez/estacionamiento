@extends('layout')
@section('content')
<h1 class="mt-4">Tipo</h1>
<ol class="breadcrumb mb-4">
	<li class="breadcrumb-item active">Tipo</li>
</ol>
<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-plus-circle"></i>
        Nuevo
    </div>
    <div class="card-body">
        <form method="POST" action="{{ route('type.store') }}">
            @csrf
            <div class="mb-3">
                <label for="type" class="form-label">Tipo</label>
                <input type="text" class="form-control" id="type" name="tipo" value="{{ old('tipo') }}" required>
                {!! $errors->first('tipo', '<span class="text-danger">:message</span>') !!}
            </div>
            <div class="mb-3">
                <label for="cost" class="form-label">Costo</label>
                <input type="number" class="form-control" id="cost" min="0" step=".01" name="costo" value="{{ old('costo') }}" required>
                {!! $errors->first('costo', '<span class="text-danger">:message</span>') !!}
            </div>
            <button type="submit" class="btn btn-primary">Enviar</button>
            <a href="{{ route('type.index') }}" class="btn btn-danger">Cancelar</a>
        </form>
    </div>
</div>
@endsection