@extends('layout')
@section('styles')
<style type="text/css">
.slot {
	height: 6rem;
	position: relative;
	cursor: pointer;
}
.slot-num {
	position: absolute;
	top: 0;
	left: 0;
}
</style>
@endsection
@section('content')
<h1 class="mt-4">Estacionamiento</h1>
<ol class="breadcrumb mb-4">
	<li class="breadcrumb-item active">Estacionamiento</li>
</ol>

<div class="row parking pb-3">
	@foreach($parkings as $parking)
	<div class="col-6 col-md-4 col-lg-3 text-center p-2 slot border {{ $parking->plate?'bg-primary':'' }}" data-slot="{{ $parking->slot }}" data-bs-toggle="modal" data-bs-target="#{{ $parking->plate?'modal2':'exampleModal'}}">
		<strong class="fs-5 slot-num p-1">{{ $parking->slot }}</strong>
		@if($parking->plate)
		<strong class="p-4 fs-4">{{ $parking->plate }}</strong>
		@endif
	</div>
	@endforeach
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"></h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<form method="POST" action="{{ route('parking.initParking') }}">
					@csrf
					<input type="hidden" id="parkigSlotId" name="slot">
					<div class="mb-3">
						<label for="placa" class="form-label">Número de placa</label>
						<input type="text" class="form-control" id="placa" name="placa">
					</div>
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary">Confirmar</button>
				</form>
			</div>
			
		</div>
	</div>
</div>


<div class="modal fade" id="modal2" tabindex="-1" aria-labelledby="modal2Label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modal2Label">Modal title</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<form id="formCobrar" method="POST" action="{{ route('parking.finishParking') }}">
					@csrf
					<input type="hidden" id="parkigSlotId2" name="slot">
					<div class="mb-3">
						<p class="fs-5">¿Deseas cobrar el tiempo de estacionamiento?</p>
						<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
						<button type="submit" class="btn btn-primary">Confirmar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@if($stay = Session::get('cobrar'))
<div class="modal fade" id="cobrarModal" tabindex="-1" aria-labelledby="cobrarModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="cobrarModalLabel">Cobrar</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<div>
					<strong>Tipo: </strong>{{$stay->type}}
				</div>
				<div>
					<strong>Minutos: </strong>{{$stay->out->diffInMinutes($stay->in)}}
				</div>
				<div>
					<strong>Total a cobrar: </strong>{{$stay->cost}}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" data-bs-dismiss="modal">Aceptar</button>
			</div>
		</div>
	</div>
</div>
@endif

@endsection
@section('scripts')
<script type="text/javascript">
	document.getElementById('exampleModal').addEventListener('show.bs.modal', function (event) {
		var slot = event.relatedTarget.dataset.slot;
		document.getElementById('exampleModalLabel').textContent = 'Slot ' + slot;
		document.getElementById('parkigSlotId').value = slot;
	});
	document.getElementById('modal2').addEventListener('show.bs.modal', function (event) {
		var slot = event.relatedTarget.dataset.slot;
		document.getElementById('modal2Label').textContent = 'Slot ' + slot;
		document.getElementById('parkigSlotId2').value = slot;
	});
	@if(Session::get('cobrar'))
	var myModal = new bootstrap.Modal(document.getElementById("cobrarModal"), {});
	document.onreadystatechange = function () {
		myModal.show();
	};
	@endif
</script>
@endsection