@extends('layout')
@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
@endsection
@section('content')
<h1 class="mt-4">Registro</h1>
<ol class="breadcrumb mb-4">
	<li class="breadcrumb-item active">Registro</li>
</ol>
<a class="btn btn-primary mb-3" href="{{ route('record.create') }}">
    <i class="fas fa-plus-circle"></i>
    Nuevo
</a>
<div class="card mb-4">
    <div class="card-body">
        <table id="record-datatable" class="table table-striped" style="width:100%">
            <thead>
                <tr>
                    <th>Placa</th>
                    <th>Tipo</th>
                    <th>Acción</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<form id="formEliminar" method="POST">
    @method('DELETE')
    @csrf
    <input type="hidden" id="id">
</form>
@endsection
@section('scripts')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script>
    $(document).ready(function() {
        var table = $('#record-datatable').DataTable({
            processing: true,
            serverSide: true,
            language: {
                "sProcessing":"Procesando...",
                "sLengthMenu":"Mostrar _MENU_ registros",
                "sZeroRecords":"No se encontraron resultados",
                "sEmptyTable":"Ningún dato disponible en esta tabla",
                "sInfo":"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":"Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":"(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":"",
                "sSearch":"Buscar:",
                "sUrl":"",
                "sInfoThousands":",",
                "sLoadingRecords":"Cargando...",
                "oPaginate": {
                    "sFirst":"Primero",
                    "sLast":"Último",
                    "sNext":"Siguiente",
                    "sPrevious":"Anterior"
                },
                "oAria": {
                    "sSortAscending":": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending":": Activar para ordenar la columna de manera descendente"
                }
            },
            ajax: '{!! route("datatable.record") !!}',
            columns: [
            {data: 'plate', name: 'plate'},
            {data: 'type.type', name: 'type.type'},
            {defaultContent: "<table><tr><td><button class='editar btn btn-warning'><i class='far fa-edit' aria-hidden='true'></i></button></td><td><button class='eliminar btn btn-danger text-dark'><i class='far fa-trash-alt' aria-hidden='true'></i></button></td></tr></table>", orderable: false, searchable: false}
            ]
        });

        $("#record-datatable tbody").on("click", "button.editar", function(){
            var row = table.row($(this).parents("tr")).data();
            location.href = "{{ route('record.index') }}" + "/" + row['id'] + "/edit";
        });
        $("#record-datatable tbody").on("click", "button.eliminar", function(){
            if(confirm("Realmente desea eliminar éste registro")){
                var row = table.row($(this).parents("tr")).data();
                $("#id").val(row["id"]);
                $("#formEliminar").attr("action","{{ route('record.index') }}"+"/"+row["id"]);
                $("#formEliminar").submit();
            }
        });

    });
</script>


@endsection