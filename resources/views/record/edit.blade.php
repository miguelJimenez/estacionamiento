@extends('layout')
@section('content')
<h1 class="mt-4">Registro</h1>
<ol class="breadcrumb mb-4">
	<li class="breadcrumb-item active">Registro</li>
</ol>
<div class="card mb-4">
    <div class="card-header">
        <i class="far fa-edit me-1"></i>
        Editar
    </div>
    <div class="card-body">
        <form method="POST" action="{{ route('record.update', $record->id) }}">
            @csrf
            @method('PUT')
            <div class="mb-3">
                <label for="placa" class="form-label">Placa</label>
                <input type="text" class="form-control" id="placa" name="placa" value="{{ $record->plate }}" required>
                {!! $errors->first('placa', '<span class="text-danger">:message</span>') !!}
            </div>
            <div class="mb-3">
                <label for="cost" class="form-label">Tipo</label>
                <select class="form-select" name="tipo" required>
                    @foreach($types as $type)
                    <option value="{{ $type->id }}" {{ $record->type->id == $type->id?'selected':'' }}>{{ $type->type }}</option>
                    @endforeach
              </select>
              {!! $errors->first('costo', '<span class="text-danger">:message</span>') !!}
          </div>
          <button type="submit" class="btn btn-primary">Editar</button>
          <a href="{{ route('record.index') }}" class="btn btn-danger">Cancelar</a>
      </form>
  </div>
</div>
@endsection