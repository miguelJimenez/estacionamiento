<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
</head>
<body>
	<table>
		<tr><td style="text-align: center; font-size: 2rem;">Estacionamiento</td></tr>
	</table>
	<table style="width: 100%">
		<thead style="border-bottom: 1px solid #000;">
			<tr>
				<th>Num. Placa</th>
				<th>Entrada</th>
				<th>Salida</th>
				<th>Tipo</th>
				<th>Pagado</th>
			</tr>
		</thead>
		<tbody>
			@foreach($stays as $stay)
			<tr>
				<td>{{ $stay->plate }}</td>
				<td>{{ $stay->in }}</td>
				<td>{{ $stay->out }}</td>
				<td>{{ $stay->type }}</td>
				<td>${{ $stay->cost }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>