<table>
    <thead>
        <tr>
            <th>Num. Placa</th>
            <th>Entrada</th>
            <th>Salida</th>
            <th>Tipo</th>
            <th>Pagado</th>
        </tr>
    </thead>
    <tbody>
        @foreach($stays as $stay)
        <tr>
            <td>{{ $stay->plate }}</td>
            <td>{{ $stay->in }}</td>
            <td>{{ $stay->out }}</td>
            <td>{{ $stay->type }}</td>
            <td>{{ $stay->cost }}</td>
        </tr>
        @endforeach
    </tbody>
</table>