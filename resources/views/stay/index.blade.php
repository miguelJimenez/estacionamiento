@extends('layout')
@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
@endsection
@section('content')
<h1 class="mt-4">Historico</h1>
<ol class="breadcrumb mb-4">
	<li class="breadcrumb-item active">Historico</li>
</ol>
<div>
    <strong>Descargar reporte</strong>
    <form class="" id="fechas" method="POST">
        @csrf
        Inicio: 
        <input type="date" id="start" name="start" min="2016-01-01" max="2030-12-31" value="{{now()->format('Y-m-d')}}" required>
        Fin:
        <input type="date" id="end" name="end" min="2016-01-01" max="2030-12-31" value="{{now()->format('Y-m-d')}}" required>
        <button type="submit" class="btn btn-success m-3" form="fechas" data-file="xls"><i class="far fa-file-excel"></i> XLSX</button>
        <button type="submit" class="btn btn-danger" form="fechas" data-file="pdf"><i class="far fa-file-pdf"></i> PDF</button>
    </form>
</div>
<div class="card mb-4">
    <div class="card-body">
        <table id="stay-datatable" class="table table-striped" style="width:100%">
            <thead>
                <tr>
                    <th>Placa</th>
                    <th>Entrada</th>
                    <th>Salida</th>
                    <th>Tipo</th>
                    <th>Cobrado</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection
@section('scripts')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
<script>
    $(document).ready(function() {
        var table = $('#stay-datatable').DataTable({
            processing: true,
            serverSide: true,
            language: {
                "sProcessing":"Procesando...",
                "sLengthMenu":"Mostrar _MENU_ registros",
                "sZeroRecords":"No se encontraron resultados",
                "sEmptyTable":"Ningún dato disponible en esta tabla",
                "sInfo":"Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":"Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":"(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":"",
                "sSearch":"Buscar:",
                "sUrl":"",
                "sInfoThousands":",",
                "sLoadingRecords":"Cargando...",
                "oPaginate": {
                    "sFirst":"Primero",
                    "sLast":"Último",
                    "sNext":"Siguiente",
                    "sPrevious":"Anterior"
                },
                "oAria": {
                    "sSortAscending":": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending":": Activar para ordenar la columna de manera descendente"
                }
            },
            ajax: '{!! route("datatable.stay") !!}',
            columns: [
            {data: 'plate', name: 'plate'},
            {data: 'in', name: 'in'},
            {data: 'out', name: 'out'},
            {data: 'type', name: 'type'},
            {data: 'cost', name: 'cost'}
            ]
        });

    });
    document.getElementById('fechas').addEventListener('submit', function(event) {
        var action = event.submitter.dataset.file;
        if (action == 'xls') {
            event.target.action = '{!! route("stay.xls") !!}';
        } else {
            event.target.action = '{!! route("stay.pdf") !!}';
        }
        
    });
</script>


@endsection