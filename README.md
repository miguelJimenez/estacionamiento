## Estacionamiento

Práctica realizada en Laravel 8.

Instalación.
- Clonar el [repositorio](https://gitlab.com/miguelJimenez/estacionamiento).
- Instalar las dependecias via composer "composer install"
- Empatar variables de entorno en .env para tener acceso a una base de datos
- Cambiar la variable PARKING_SLOTS de .env para cambiar el numero de espacios para el estacionamiento.
- Correr las migraciones y los seeds "php artisan migrate --seed"
- Opcionalmente correr el servidor de artisan "php artisan serve"

El sistema cuenta con 4 modelos:
- Type: Describe que tipo de usuarios puede haber, y la cuota para cada tipo de usuario.
- Record: Guarda el número de placas y el tipo de usuario.
- Parking: Guarda el estado del estacionamiento, cuenta con el numero de plazas que se describe en la variable de entorno "PARKING_SLOTS".
-Stay: Contiene el historial de todos los vehículos que han entrado al estacionamiento (la DDBB carga 50 registros mediante un Factory).

En el index del sitio se muestra una cuadricula ejemplificando los espacios del estacionamiento, cada cuadro tienesu numero de espacio, se debe hacer click encima del cuadro para que salga un "Dialog" en donde se ingresa el número de placa. al salir el vehículo se vuelve a hacer click sobre el cuadro, donde se pregunta si quieres cobrar al dar "Aceptar" se desocupa el espacio y sevisualiza el monto a cobrar segun sus placas.
