<?php

use App\Http\Controllers\Parking\FinishParkingController;
use App\Http\Controllers\Parking\InitParkingController;
use App\Http\Controllers\Parking\ParkingIndexController;
use App\Http\Controllers\RecordController;
use App\Http\Controllers\Stay\IndexStayController;
use App\Http\Controllers\Stay\ReportStayController;
use App\Http\Controllers\TypeController;
use App\Http\Controllers\datatables\RecordDatatable;
use App\Http\Controllers\datatables\StayDatatable;
use App\Http\Controllers\datatables\TypeDatatable;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ParkingIndexController::class, 'index'])->name('index');

Route::post('parking/init', [InitParkingController::class, 'initParking'])->name('parking.initParking');

Route::post('parking/finish', [FinishParkingController::class, 'finishParking'])->name('parking.finishParking');

Route::get('stay', [IndexStayController::class, 'index'])->name('stay.index');

Route::post('stay/xls', [ReportStayController::class, 'downloadXls'])->name('stay.xls');

Route::post('stay/pdf', [ReportStayController::class, 'downloadPdf'])->name('stay.pdf');

Route::resource('type', TypeController::class)->except(['show']);

Route::resource('record', RecordController::class)->except(['show']);

Route::prefix('datatable')->group(function () {
    Route::get('type', [TypeDatatable::class, 'typeDatatable'])->name('datatable.type');
    Route::get('record', [RecordDatatable::class, 'recordDatatable'])->name('datatable.record');
    Route::get('stay', [StayDatatable::class, 'stayDatatable'])->name('datatable.stay');
});
